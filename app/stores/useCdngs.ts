import { ICdngInfo } from '@/components/utils/types/ICdng';
import { create } from 'zustand';

export interface ICdngStore {
    cdngs: Array<ICdngInfo>,
    addCdng: (data: ICdngInfo) => void
}

export const useCdngs = create<ICdngStore>(set => ({

    cdngs: [
        {
            master: '',
            masterPhone: '',
            uid: '',
            name: ''
        }
    ],

    addCdng: (data: ICdngInfo) => set(state => {
        return {
            cdngs: [
                ...state.cdngs,
                data
            ]
        }
    })
}))
