import LoginForm from '@/components/features/loginForm/LoginForm';
import type { Metadata } from 'next';

export const metadata: Metadata = {
  title: 'Вход',
  description: 'Авторизация'
};

export default function Login() {
  return (
    <main>
      <LoginForm />
    </main>
  );
}
