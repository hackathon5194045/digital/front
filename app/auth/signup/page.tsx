import SignUpForm from '@/components/features/signUpForm/SignUpForm';

import type { Metadata } from 'next';

export const metadata: Metadata = {
  title: 'Регистрация'
};

export default function SignUp() {
  return (
    <main>
      <SignUpForm />
    </main>
  );
}
