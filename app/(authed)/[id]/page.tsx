import Cdng from '@/components/features/cdng/Cdng';
import { getCdngByuid } from '@/components/utils/services/getCdng';
import { ICdngInfo } from '@types';

const CdngPage = async ({ params }: { params: { id: string } }) => {
  return (
    <main>
      <Cdng uid={params.id} />
    </main>
  );
};

export default CdngPage;
