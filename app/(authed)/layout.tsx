import type { Metadata } from 'next';
import MainHeader from '@/components/features/headers/MainHeader';
import MainNavbar from '@/components/features/navbars/MainNavbar';
import MainFooter from '@/components/features/footers/MainFooter';
import classes from './mainPage.module.css'

export const metadata: Metadata = {
  title: 'Digital',
  description: 'Digital'
};

export default function MainLayout({ children }: { children: React.ReactNode }) {

  return (
    <div className={classes.wrapper}>
      <MainHeader />
      <div className={classes.navbar}>
        <MainNavbar />
      </div>
      <div className={classes.chidren_contantainer}>
        {children}
      </div>
      <div className={classes.footer}>
        <MainFooter />
      </div>
    </div>
  );
}
