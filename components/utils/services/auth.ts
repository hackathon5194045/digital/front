import { baseUrl } from '@constants';
import type { ILoginUser } from '@types';
import type { JWT } from 'next-auth/jwt';

export async function registration(data: ILoginUser) {
  const res = await fetch(`${baseUrl}/auth/reg`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  });
  return res;
}

export async function refreshToken(token: JWT): Promise<JWT> {
  const res = await fetch(`${baseUrl}/auth/refresh`, {
    method: 'POST',
    headers: {
      authorization: `Refresh ${token.tokens.refreshToken}`
    }
  });

  const response = await res.json();

  return {
    ...token,
    tokens: response
  };
}
