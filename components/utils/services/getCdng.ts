import { getSession } from 'next-auth/react';
import { baseUrl } from '../constants';

export const getCdng = async () => {
  const session = await getSession();
  const res = await fetch(`${baseUrl}/cdng`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${session?.tokens.accessToken}`
    }
  });
  return res.json();
};

export const createCdng = async () => {
  const session = await getSession();
  const res = await fetch(`${baseUrl}/cdng`, {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${session?.tokens.accessToken}`
    }
  });
  return res.json();
};

export const getCdngByuid = async (uid: string) => {
  const session = await getSession();
  const res = await fetch(`${baseUrl}/cdng/${uid}`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${session?.tokens.accessToken}`
    }
  });
  return res.json();
};

export const getDataForChart = async (uid: string) => {
  const res = await fetch(`${baseUrl}/cdng/graph/${uid}`, {
    method: 'GET'
  });
  return res.json();
};
