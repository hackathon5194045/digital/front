import { getSession } from 'next-auth/react';
import { baseUrl } from '../constants';
import { IDtoPlan, IUpdateDtoPlan } from '../types';

export const getPlan = async (uid: string, dto: IDtoPlan) => {
  const res = await fetch(`${baseUrl}/plan/${uid}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(dto)
  });

  return res.json();
};

export const updatePlan = async (uid: string, dto: IUpdateDtoPlan) => {
  const session = await getSession();
  const res = await fetch(`${baseUrl}/plan/${uid}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${session?.tokens.accessToken}`
    },
    body: JSON.stringify(dto)
  });

  return res.json();
};
