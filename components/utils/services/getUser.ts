import { getSession } from "next-auth/react";
import { baseUrl } from "../constants";

export const getUser = async () => {
    const session = await getSession()
    const res = await fetch(`${baseUrl}/users`, {
        method: 'GET',
        headers: {
            Authorization: `Bearer ${session?.tokens.accessToken}`
        },
    });
    return res.json();
}
