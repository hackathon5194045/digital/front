export interface ICdng {
  uid: string;
  name: string;
}

export interface ICdngInfo extends ICdng {
  master: string;
  masterPhone: string;
}

export interface IChartData {
  factData: Array<IFactData>;
  planData: Array<IPlanData>;
}

export interface IFactData {
  id: number;
  time: number;
  calculated_debit: number;
}

export interface IPlanData {
  id: number;
  time: number;
  data: number;
}
