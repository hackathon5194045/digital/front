export interface ISignUpUser {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}

export interface ILoginUser {
  email: string;
  password: string;
}

export interface IUser {
  id: number,
  firstName: string;
  lastName: string;
}
