export interface IDtoPlan {
  day: number;
  month: number;
}

export interface IUpdateDtoPlan extends IDtoPlan {
  newDebit: number;
}

export interface IPlan {
  totalDebit: number;
  plannedDebit: number;
  history: Array<IHistory>;
}

export interface IHistory {
  id: number;
  plannedDebit: number;
  time: string;
  adjustment: number;
  editor: Array<IEditor>;
  result: number;
}

export interface IEditor {
  id: number;
  firstName: string;
  lastName: string;
  edited: number;
}
