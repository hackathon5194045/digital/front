import { createTheme } from '@mantine/core';

export const theme = createTheme({
  defaultRadius: 'xs',
  colors: {
    gray: [
      '#fef2f5',
      '#eae6e7',
      '#cdcdcd',
      '#b2b2b2',
      '#9a9a9a',
      '#8b8b8b',
      '#848484',
      '#717171',
      '#676465',
      '#5e5457'
    ],
    cyan: [
      '#ebfaff',
      '#d7f3fb',
      '#a9e6f9',
      '#7ad9f8',
      '#5dcef6',
      '#4ec7f6',
      '#46c4f7',
      '#39addc',
      '#2a9ac5',
      '#0085ad'
    ],
    brown: [
      '#fff4e7',
      '#f1e8db',
      '#decfbc',
      '#c9b49b',
      '#b79e7d',
      '#ad906a',
      '#a88860',
      '#93754f',
      '#846743',
      '#745935'
    ],
    yellow: [
      '#fff8e0',
      '#fff0ca',
      '#ffdf9a',
      '#fdcd65',
      '#fcbe38',
      '#fcb51b',
      '#fcb007',
      '#e19a00',
      '#c88900',
      '#ad7500'
    ],
    darkBlue: [
      '#ecf5fd',
      '#d9e7f6',
      '#acceee',
      '#7eb4e9',
      '#5a9ee3',
      '#4690e1',
      '#3a89e1',
      '#2d76c8',
      '#2469b3',
      '#0f5a9e'
    ]
  },
  primaryColor: 'darkBlue',
  fontFamily: 'Roboto, Arial, Helvetica, Microsoft Sans Serif'
});
