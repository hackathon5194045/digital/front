'use client';

import { Container, Tooltip, UnstyledButton } from '@mantine/core';
import { useRouter } from 'next/navigation';
import classes from './MainNavbar.module.css';
import { useEffect, useState } from 'react';
import { ICdng } from '../../utils/types/ICdng';
import { createCdng, getCdng } from '../../utils/services/getCdng';

const MainNavbar = ({ params }: { params: { uid: string } }) => {
  const [active, setActive] = useState<number>(-1);
  let [cdngs, setCdngs] = useState<ICdng[]>();

  const router = useRouter();

  useEffect(() => {
    fetchCdng();
  }, []);

  const fetchCdng = async () => {
    const res: ICdng[] = await getCdng();
    setCdngs((cdngs = res));
  };

  const createDa = async () => {
    await createCdng();
    await fetchCdng();
  };

  const selectCdng = (index: number, uid: string) => {
    setActive(index);
    router.push(`/${uid}`);
  };

  return (
    <Container size='xl'>
      <div className={classes.nav_container}>
        <div className={classes.left_part_sidebar}>
          {cdngs !== undefined &&
            cdngs.map((cdng, index) => (
              <Tooltip key={cdng.uid} label={cdng.name} transitionProps={{ duration: 0 }}>
                <UnstyledButton
                  onClick={() => selectCdng(index, cdng.uid)}
                  className={`${classes.link} ${active == index ? classes.link_active : ''}`}
                  data-active={active || undefined}
                >
                  {cdng.name}
                </UnstyledButton>
              </Tooltip>
            ))}
        </div>
        <button onClick={() => createDa()} className={classes.add_Cdng}>
          Добавить ЦДНГ <span className={classes.plus}>+</span>
        </button>
      </div>
    </Container>
  );
};

export default MainNavbar;
