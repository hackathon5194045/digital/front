'use client';

import Link from 'next/link';
import {
  TextInput,
  PasswordInput,
  Anchor,
  Paper,
  Text,
  Container,
  Button,
  Stack
} from '@mantine/core';
import { useForm } from '@mantine/form';
import { ISignUpUser } from '@types';
import { registration } from '@services';
import { signIn } from 'next-auth/react';
import { useRouter } from 'next/navigation';
import Logo from '@/public/logo.svg';
import Image from 'next/image';

const SignUpForm = () => {
  const form = useForm<ISignUpUser>({
    initialValues: { firstName: '', lastName: '', email: '', password: '' },
    validate: {
      email: (value) => (/^\S+@\S+$/.test(value) ? null : 'Invalid email')
    }
  });
  const router = useRouter();

  const handleSubmit = async (values: ISignUpUser) => {
    const res = await registration(values);
    if (res.ok) {
      await signIn('credentials', {
        email: values.email,
        password: values.password,
        redirect: false
      });
      router.replace('/');
    }
  };

  return (
    <Container size={420} my={40}>
      <Paper withBorder shadow='md' p={30} mt={30}>
        <Stack align='center'>
          <Image src={Logo} alt='' />
        </Stack>
        <form onSubmit={form.onSubmit((values) => handleSubmit(values))}>
          <TextInput
            label='Имя'
            placeholder='Ваше имя'
            required
            {...form.getInputProps('firstName')}
          />
          <TextInput
            label='Фамилия'
            placeholder='Ваша фамилия'
            required
            mt='md'
            {...form.getInputProps('lastName')}
          />
          <TextInput
            label='Email'
            placeholder='example@sng.ru'
            required
            mt='md'
            {...form.getInputProps('email')}
          />
          <PasswordInput
            label='Пароль'
            placeholder='Ваш пароль'
            required
            mt='md'
            {...form.getInputProps('password')}
          />
          <Button type='submit' fullWidth mt='xl'>
            Зарегистрироваться
          </Button>
        </form>
      </Paper>
      <Text c='dimmed' size='sm' ta='center' mt={5}>
        <Anchor size='sm' href={'/auth/login'} component={Link}>
          Войти
        </Anchor>
      </Text>
    </Container>
  );
};

export default SignUpForm;
