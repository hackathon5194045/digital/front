import { Container } from "@mantine/core"
import classes from './MainFooter.module.css'

const MainFooter = () => {
    return (
        <div className={classes.footer}>
            <Container size="lg" mt={150}>
                <div className={classes.content}>
                    <a className={classes.link} href="https://www.surgutneftegas.ru/tos/">Страница ПАО «Сургутнефтегаз» на сайте информационного агентства и Заявление об ответственности.</a>
                    <p className={classes.link}>© ПАО «Сургутнефтегаз» Все права сайта защищены</p>
                </div>
            </Container>
        </div>
    )
}

export default MainFooter

