'use client';

import { useEffect, useState } from 'react';
import Image from 'next/image';
import Link from 'next/link';
import Logo from '@/components/styles/icons/logo.svg';
import { Button } from '@mantine/core';
import { Container, Group, Text } from '@mantine/core';
import classes from './MainHeader.module.css';
import { getUser } from '@/components/utils/services/getUser';
import { IUser } from '@/components/utils/types';
import { signOut } from 'next-auth/react';

const user = {
  id: 0,
  firstName: '',
  lastName: ''
};

const MainHeader = () => {
  const [usr, setUsr] = useState<IUser>(user);

  useEffect(() => {
    fetchUser();
  }, []);

  const fetchUser = async () => {
    const res: IUser = await getUser();
    setUsr(res);
  };
  return (
    <div className={classes.header}>
      <Container className={classes.mainSection} size='xl'>
        <Group justify='space-between'>
          <Link href={'/'}>
            <Image src={Logo} alt={'SNG'} />
          </Link>
          <Group gap={'xl'}>
            <Text fw={700} tt={'uppercase'} size='md' c='white' lh={1}>
              {usr.lastName + ' ' + usr.firstName}
            </Text>
            <Button
              onClick={() => signOut()}
              className={classes.exit_btn}
              variant='subtle'
              size='compact-md'
            >
              <Text c='white'>Выйти</Text>
            </Button>
          </Group>
        </Group>
      </Container>
    </div>
  );
};

export default MainHeader;
