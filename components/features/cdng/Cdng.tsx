import Chart from './Chart';
import Info from './Info';
import Plans from './Plans';

import { Container, Grid, GridCol, Stack } from '@mantine/core';
import { ICdngInfo, IPlan } from '@types';
import { getCdngByuid, getPlan } from '@services';

const Cdng = async ({ uid }: { uid: string }) => {
  const res: ICdngInfo = await getCdngByuid(uid);
  const plan: IPlan = await getPlan(uid, { day: 19, month: 11 });
  return (
    <Container size={'xl'} mt={'xl'}>
      <Stack>
        <Grid>
          <GridCol span={4}>
            <Info {...res} />
          </GridCol>
          <GridCol span={8}>
            <Plans plan={plan} uid={uid} />
          </GridCol>
        </Grid>
        <Chart uid={uid} />
      </Stack>
    </Container>
  );
};

export default Cdng;
