import { Group, Stack, Text } from '@mantine/core';
import Image from 'next/image';
import Map from '@/components/styles/icons/map.svg';
import { ICdngInfo } from '@types';

const Info = ({ name, master, masterPhone }: ICdngInfo) => {
  return (
    <Stack bg={'#F7F7F7'} gap={'md'} p={'md'}>
      <Text c={'#0C487E'} fw={700}>
        ИНФОРМАЦИЯ
      </Text>

      <Stack>
        <Text fw={700}>Название ЦДНГ</Text>
        <Text p={'xs'}>{name}</Text>
      </Stack>

      <Stack>
        <Text fw={700}>Расположение</Text>
        <Stack p={'xs'}>
          <Text>Нефтеюганское шоссе, 573 километр</Text>
          <Group justify='flex-end'>
            <Text c={'#AD906B'}>Посмотреть на карте</Text>
            <Image src={Map} alt='' />
          </Group>
        </Stack>
      </Stack>

      <Stack>
        <Text fw={700}>ФИО</Text>
        <Text p={'xs'}>{master}</Text>
      </Stack>

      <Stack>
        <Text fw={700}>Номер телефона</Text>
        <Text p={'xs'}>{masterPhone}</Text>
      </Stack>
    </Stack>
  );
};

export default Info;
