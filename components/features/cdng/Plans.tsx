'use client';
import 'dayjs/locale/ru';
import '@mantine/dates/styles.css';
import customParseFormat from 'dayjs/plugin/customParseFormat';
import {
  Button,
  Center,
  Group,
  Input,
  Popover,
  PopoverDropdown,
  PopoverTarget,
  Stack,
  Table,
  TableTbody,
  TableTd,
  TableTh,
  TableThead,
  TableTr,
  Tabs,
  TabsList,
  TabsTab,
  Text
} from '@mantine/core';
import Image from 'next/image';
import Calendar from '@/components/styles/icons/calendar.svg';
import Edit from '@/components/styles/icons/edit.svg';
import { useState } from 'react';
import { DatePicker } from '@mantine/dates';
import classes from './Plans.module.css';
import dayjs from 'dayjs';
import { IPlan, IUpdateDtoPlan } from '@types';
import { useForm } from '@mantine/form';
import { updatePlan } from '@services';
import { useRouter } from 'next/navigation';

dayjs.extend(customParseFormat);

const tableTitles = [
  '№',
  'ФИО, вносившего корректировку',
  'Время',
  'План',
  'Корректировка плана',
  'Итог'
];

const today = new Date();

const Plans = ({ uid, plan }: { uid: string; plan: IPlan }) => {
  const form = useForm<IUpdateDtoPlan>({
    initialValues: { newDebit: 0, day: 19, month: 11 }
  });

  const handleUpdate = async (values: IUpdateDtoPlan) => {
    await updatePlan(uid, values);
    router.refresh();
  };

  const router = useRouter();

  const [date, setDate] = useState(new Date());

  const weekArray = Array.from({ length: 7 }, (_, index) => {
    const currentDate = new Date();
    // if (date > new Date(date.getDate() + 7)) {
    //   currentDate.setDate(date?.getDate() + index);
    //   return currentDate;
    // }
    currentDate.setDate(today.getDate() + index);
    return currentDate;
  });

  const handleTabClick = (selectedDate: string) => {
    const parsedDate = parseInt(selectedDate);
    const newDate = new Date(date || today);
    newDate.setDate(parsedDate);
    setDate(newDate);
  };

  return (
    <Stack bg={'#F7F7F7'} gap={'md'} p={'md'}>
      <Text c={'#0C487E'} fw={700}>
        ПЛАНЫ
      </Text>
      {/* календарь */}
      <Popover>
        <PopoverTarget>
          <Group justify='flex-end'>
            <Button variant='subtle'>
              <Group>
                <Text fw={700} size='sm' c={'#0C487E'}>
                  Открыть календарь
                </Text>
                <Image src={Calendar} alt='' />
              </Group>
            </Button>
          </Group>
        </PopoverTarget>
        <PopoverDropdown>
          <DatePicker
            value={date}
            onChange={setDate}
            locale='ru'
            monthsListFormat='MMM'
            yearsListFormat='YYYY'
          />
        </PopoverDropdown>
      </Popover>

      {/* ПОЛОСКA */}
      <Tabs color={'#AD906B'} value={date?.getDate().toString()} onChange={handleTabClick}>
        <TabsList>
          {weekArray.map((day) => (
            <TabsTab key={day.getDay()} value={day.getDate().toString()}>
              <Stack>
                <Text c={'#8F929B'}>{dayjs(day).locale('ru').format('dd')}</Text>
                <Text c={'#8F929B'}>
                  {dayjs(day).locale('ru').format('DD')} {dayjs(day).locale('ru').format('MMM')}
                </Text>
              </Stack>
            </TabsTab>
          ))}
        </TabsList>
      </Tabs>

      {/* Внести корректировку */}
      <Group>
        <Stack justify='flex-end'>
          <div className={classes.box}>
            <Text
              className={
                (plan.plannedDebit / plan.totalDebit) * 100 < 95 ? classes.red : classes.green
              }
              fw={'700'}
              size='lg'
            >
              {plan.totalDebit}
            </Text>
          </div>
          <Text size='xs'>Фактическое значение</Text>
        </Stack>

        <Stack justify='flex-end'>
          <div className={classes.box}>
            <Text fw={'700'} size='lg'>
              {plan.plannedDebit}
            </Text>
          </div>
          <Text size='xs'>Плановое значение</Text>
        </Stack>

        <Stack justify='flex-end'>
          <div className={classes.box}>
            <Text
              fw={'700'}
              size='lg'
              className={plan.totalDebit - plan.plannedDebit > 0 ? classes.green : classes.red}
            >
              {Math.floor(plan.totalDebit - plan.plannedDebit)}
            </Text>
          </div>
          <Text size='xs'>Дельта</Text>
        </Stack>

        {/* fix disabled={today > date} */}
        <form onSubmit={form.onSubmit((values) => handleUpdate(values))}>
          <Stack ml={'xl'}>
            <Button variant='subtle' type='submit'>
              <Group>
                <Text fw={700} size='sm' c={'#0C487E'}>
                  Внести корректировку плана
                </Text>
                <Image src={Edit} alt='' />
              </Group>
            </Button>
            <Input placeholder='Внести корректировку' {...form.getInputProps('newDebit')} />
          </Stack>
        </form>
      </Group>

      <Text fw={900} size='sm'>
        Последние корректировки
      </Text>

      <Table withColumnBorders>
        <TableThead>
          <TableTr>
            {tableTitles.map((title) => (
              <TableTh key={title} className={classes.tableTitle}>
                {title}
              </TableTh>
            ))}
          </TableTr>
        </TableThead>
        <TableTbody>
          {plan.history.map((row, index) => (
            <TableTr key={row.id}>
              <TableTd className={classes.tableValue}>{index + 1}</TableTd>
              <TableTd className={classes.tableValue}>
                {row.editor[0].firstName + ' ' + row.editor[0].lastName}
              </TableTd>
              <TableTd className={classes.tableValue}>
                {/* {dayjs(row.time).format('HH:MM')} */}
                {new Date(parseFloat(row.time)).toLocaleTimeString('ru-RU', {
                  hour: '2-digit',
                  minute: '2-digit'
                })}
              </TableTd>
              <TableTd className={classes.tableValue}>{row.plannedDebit}</TableTd>
              <TableTd
                className={
                  row.plannedDebit < row.result ? classes.tableValuePlus : classes.tableValueMinus
                }
              >
                {row.plannedDebit < row.result ? '+' + row.adjustment : row.adjustment}
              </TableTd>
              <TableTd className={classes.tableValue}>{row.result}</TableTd>
            </TableTr>
          ))}
        </TableTbody>
      </Table>
    </Stack>
  );
};

export default Plans;
