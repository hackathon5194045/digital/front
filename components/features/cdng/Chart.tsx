'use client';

import { Stack, Text } from '@mantine/core';

import React, { useEffect, useState } from 'react';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title as ChartTitle,
  Tooltip,
  Legend
} from 'chart.js';
import { Line } from 'react-chartjs-2';
import dayjs from 'dayjs';
import { getDataForChart } from '@services';
import { IChartData } from '@types';
import { useRouter } from 'next/navigation';

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  ChartTitle,
  Tooltip,
  Legend
);

const options = {
  responsive: true,
  plugins: {
    legend: {
      position: 'top' as const
    }
  }
};

const normalizeDate = (res) => {
  return new Date(res).toLocaleTimeString('ru-RU', {
    hour: '2-digit',
    minute: '2-digit'
  });
};

const Chart = ({ uid }: { uid: string }) => {
  const [chartData, setChartData] = useState<IChartData | null>(null);
  const router = useRouter();

  const getPlan = async (uid: string) => {
    const res: IChartData = await getDataForChart(uid);
    setChartData(res);
  };
  useEffect(() => {
    getPlan(uid);
  }, []);

  const labels = (() => {
    const hoursArray = chartData?.factData.map(({ time }) => time);
    const secArr = chartData?.planData.map(({ time }) => time);
    hoursArray?.push(...secArr);
    hoursArray?.sort();
    const result = hoursArray?.map((res) =>
      new Date(res).toLocaleTimeString('ru-RU', {
        hour: '2-digit',
        minute: '2-digit'
      })
    );
    return result;
  })();

  const data = {
    labels,
    datasets: [
      {
        label: 'Добыто',
        data: chartData?.factData.map((deb) => {
          return {
            x: normalizeDate(deb.time),
            y: deb.calculated_debit
          };
        }),
        borderColor: 'rgb(0,113,114)',
        backgroundColor: 'rgba(0,113,114, 0.5)'
      },
      {
        label: 'План',
        data: chartData?.planData.map((deb) => {
          return {
            x: normalizeDate(deb.time),
            y: deb.data
          };
        }),
        borderColor: 'rgb(242,183,5)',
        backgroundColor: 'rgba(242,183,5, 0.5)'
      }
      // {
      //   label: 'Прогноз',
      //   data: ['103', '114', '114', '114'],
      //   borderColor: 'rgb(3,62,140)',
      //   backgroundColor: 'rgba(3,62,140, 0.5)'
      // }
    ]
  };
  return (
    <Stack bg={'#F7F7F7'} p={'md'}>
      <Text c={'#0C487E'} fw={700}>
        РЕЗУЛЬТАТЫ
      </Text>

      <Line options={options} data={data} />
    </Stack>
  );
};

export default Chart;
