export const Predict = (oilProductionLastWeek: number[], oilProductionLastHour: number[]) => {
  // Функция для вычисления коэффициентов линейной регрессии
  function linearRegression(x: number[], y: number[]) {
    const n = x.length;
    let sumX = 0,
      sumY = 0,
      sumXY = 0,
      sumXX = 0;

    for (let i = 0; i < n; i++) {
      sumX += x[i];
      sumY += y[i];
      sumXY += x[i] * y[i];
      sumXX += x[i] * x[i];
    }

    const slope = (n * sumXY - sumX * sumY) / (n * sumXX - sumX * sumX);
    const intercept = (sumY - slope * sumX) / n;

    return { slope, intercept };
  }

  // Вычисление коэффициентов линейной регрессии
  const { slope, intercept } = linearRegression(oilProductionLastHour, oilProductionLastWeek);

  // Предсказание будущей добычи на каждый час дня
  const predictedOilProductionDay = [];
  for (let hour = 1; hour <= 24; hour++) {
    const predictedOilProduction =
      slope * oilProductionLastHour[oilProductionLastHour.length - 1] + intercept;
    predictedOilProductionDay.push(predictedOilProduction);
    oilProductionLastHour.push(predictedOilProduction); // Добавляем предсказанное значение для следующего часа
  }

  return predictedOilProductionDay;
};
