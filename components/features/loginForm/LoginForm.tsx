'use client';

import Link from 'next/link';
import {
  TextInput,
  PasswordInput,
  Anchor,
  Paper,
  Text,
  Container,
  Group,
  Button,
  Stack
} from '@mantine/core';
import { useForm } from '@mantine/form';
import { ILoginUser } from '@types';
import { signIn } from 'next-auth/react';
import { useRouter } from 'next/navigation';
import Logo from '@/public/logo.svg';
import Image from 'next/image';

const LoginForm = () => {
  const form = useForm<ILoginUser>({
    initialValues: { email: '', password: '' },
    validate: {
      email: (value) => (/^\S+@\S+$/.test(value) ? null : 'Invalid email')
    }
  });

  const router = useRouter();

  const handleSubmit = async (values: ILoginUser) => {
    const res = await signIn('credentials', {
      email: values.email,
      password: values.password,
      redirect: false
    });
    if (res?.ok) {
      router.replace('/');
    }
  };

  return (
    <Container size={420} my={40}>
      <Paper withBorder shadow='md' p={30} mt={30}>
        <Stack align='center'>
          <Image src={Logo} alt='' />
        </Stack>

        <form onSubmit={form.onSubmit((values) => handleSubmit(values))}>
          <TextInput
            label='Email'
            placeholder='example@sng.ru'
            required
            {...form.getInputProps('email')}
          />
          <PasswordInput
            label='Пароль'
            placeholder='Ваш пароль'
            required
            mt='md'
            {...form.getInputProps('password')}
          />
          <Group justify='space-between' mt='lg'>
            <Anchor component='button' size='sm'>
              Забыли пароль?
            </Anchor>
          </Group>
          <Button type='submit' fullWidth mt='xl'>
            Войти
          </Button>
        </form>
      </Paper>

      <Text c='dimmed' size='sm' ta='center' mt={5}>
        <Anchor size='sm' href={'/auth/signup'} component={Link}>
          Зарегистрироваться
        </Anchor>
      </Text>
    </Container>
  );
};

export default LoginForm;
